# Rust Advent Of Code 2022

This code is for solving the [Advent of Code 2022](https://adventofcode.com/2022) in order to refresh Rust skills and serve as demonstration code for others.