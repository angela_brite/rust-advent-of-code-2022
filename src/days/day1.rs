use crate::common::lines_from_file;

pub fn run(part: u32) {
    match part
    {
        1 => part_one(),
        2 => part_two(),
        _ => println!("Unknown part for day 1!"),
    }
}

fn part_one() {
    let input = lines_from_file("data/day1_input.txt").unwrap();
    let max: u32 = input.split(|x| x.is_empty())
        .map(|x| x.into_iter()
            .map(|x| x.parse::<u32>().unwrap())
            .sum()
    ).max().unwrap();

    println!("Maximum calories: {}", max);
}

fn part_two() {
    let input = lines_from_file("data/day1_input.txt").unwrap();
    let mut calories = input.split(|x| x.is_empty())
        .map(|x| x.into_iter()
            .map(|x| x.parse::<u32>().unwrap())
            .sum::<u32>()
        ).collect::<Vec<u32>>();
    calories.sort_unstable();
    let total_max_three = calories.into_iter().rev().take(3).sum::<u32>();
    
    println!("Total top 3 calories of elves: {}", total_max_three);
}